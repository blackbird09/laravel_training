<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Register part
Route::post('/register', 'Auth\AuthController@postRegister');
Route::get('/welcome', function() {
    return view('created');
});
//Login part
Route::post('/auth', 'Auth\AuthController@postLogin');
Route::get('/loggedin', function() {
    return view('loggedin');
});
//Logout part
Route::get('/logout', 'Auth\AuthController@getLogout');