<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Laravel 5</div>
                <form method="POST" action={{url('/auth')}}>
                    {!! csrf_field() !!}
                    @if(count($errors)>0)
                        <div>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <p><span>Username: </span><input type="text" name="username" /></p>
                    <p><span>Password: </span><input type="password" name="password" /></p>
                    <button type="submit">Login</button>
                </form>
                <form method="POST" action={{url('/register')}}>
                    {!! csrf_field() !!}
                    @if(count($errors)>0)
                        <div>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <p><span>Username: </span><input type="text" name="username" /></p>
                    <p><span>Password: </span><input type="password" name="password" /></p>
                    <button type="submit">Create user</button>
                </form>
            </div>
        </div>
    </body>
</html>
